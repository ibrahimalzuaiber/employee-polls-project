import { connect, useDispatch} from "react-redux";
import Header from "../components/header/Header";
import "./style.css";
import { useLocation } from "react-router-dom";
import { setLocation } from "../redux/actions/index";
import Login from "../pages/Login";
import  Lang  from "../constants/Lang";

function Leaderboard(props)
{
    const location = useLocation();
    const dispatch = useDispatch();

    if(props.authedUser === null)
    {
        dispatch(setLocation(location.pathname));
        return(
            <Login/>
        )
    }

    return(
        <div>
          <Header />
            <div className="leaderboard-container">
            <table>
              <tr>
                <th>{Lang.avatar}</th>
                <th>{Lang.UserName}</th>
                <th>{Lang.Answered}</th>
                <th>{Lang.Created}</th>
              </tr>
        {props.users.map((val, key) => {
          return (
            <tr key={key}>
              <td>
                <img src={val.avatarURL} className="leaderboard-table-avatar" alt='user-avatar'/>
              </td>
              <td>{val.name}</td>
              <td>{Object.keys(val.answers).length}</td>
              <td>{val.questions.length}</td>
            </tr>
          )
        })}
      </table>
            </div>
        </div>
    )
}

function mapStateToProps({authedUser, users})
{
    if(authedUser === null)
    {
        return{
            authedUser: null
        }
    }

    return{
        users: Object.values(users).sort((a, b) => {
           return (Object.keys(b.answers).length + b.questions.length) - (Object.keys(a.answers).length + a.questions.length)
        })
    }

}

export default connect(mapStateToProps)(Leaderboard);