import "./style.css";
import Card from "../components/card/Card";
import Header from "../components/header/Header";
import Footer from "../components/footer/Footer";
import { connect, useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import { setLocation } from "../redux/actions/index";
import Login from "./Login";
import  Lang  from "../constants/Lang";
import userReducer from "../redux/reducers/user";
import { useState } from "react";


function Home(props,{ children }) {
  const dispatch = useDispatch();
  const location = useLocation();
  const [show, setShow] = useState();

  // function to toggle the boolean value
  function toggleShow() {
    setShow(!show);
  }
  var buttonText = show ? " 🔽 Hide Answered Polls" : " 🔼 Show Answered Polls";

  
  if (props.authedUser === null) {
    dispatch(setLocation(location.pathname));
    return <Login />;
  }

  function newQuestions() {
    let unansweredQuestions = [];
    let answeredQuestions = [];
    props.questions.forEach((element) => {
      props.answers.includes(element.id)
        ? answeredQuestions.push(element)
        : unansweredQuestions.push(element);
    });

    return {
      answered: answeredQuestions,
      unanswered: unansweredQuestions,
    };
  }
  let filteredQuestions = newQuestions();
  

  return (
    <div>
      <Header />
     
      <div className="home-wrapper">
        <div className="home-card-header">
          <h1>{Lang.NewQuestion}</h1>
          <div className="home-card-items">
            {filteredQuestions.unanswered.map((element) => {
              return <Card question={element} key={element.id} />;
            })}
          </div>
        </div>
        <div>
        {show && children}
            <div onClick={toggleShow}>
              <h3 className="home-hide-text">{buttonText}</h3>
            </div>
        </div>
        { buttonText === ' 🔼 Show Answered Polls' ? '':(<>
        <div className="home-card-wrapper">
          <div className="home-card-header">
            <h1>{Lang.done}</h1>
          </div>
          <div className="home-card-items">
            {filteredQuestions.answered.map((element) => {
              console.log(element.author);
              console.log(userReducer);
              return <Card question={element} key={element.id} />;
            })}
          </div>
        </div>
        </>)}
      </div>
     
    
   
      <Footer />
    </div>
  );
}

function mapStateToProps({ authedUser, questions }) {
  if (authedUser === null) {
    return {
      authedUser: null,
    };
  }

  return {
    answers:
      authedUser.answers !== null ? Object.keys(authedUser.answers) : null,
    questions: questions.sort((a, b) => {
      return b.timestamp - a.timestamp;
    }),
  };
}

export default connect(mapStateToProps)(Home);
