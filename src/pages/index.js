export { default as Login } from "./Login";
export { default as Leaderboard } from "./Leaderboard";
export { default as Home } from "./Home";
export { default as NewPoll } from "./NewPoll";
export { default as Poll } from "./Poll";
export { default as PageNotFound } from "./PageNotFound";

