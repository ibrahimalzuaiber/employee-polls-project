import { connect, useDispatch } from "react-redux";
import Header from "../components/header/Header";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import "./style.css";
import { handleQuestionAnswer } from "../redux/actions/questions";
import { setPollOption } from "../redux/actions/authUser";
import { PieChart, Pie, ResponsiveContainer, Legend } from "recharts";
import { setLocation, setUserAnswer } from "../redux/actions/index";
import Login from "./Login";
import  Lang  from "../constants/Lang";

const withRouter = (Component) => {
  const ComponentWithRouterProp = (props) => {
    let location = useLocation();
    let navigate = useNavigate();
    let params = useParams();
    return <Component {...props} router={{ location, navigate, params }} />;
  };

  return ComponentWithRouterProp;
};

function Poll(props) {
  console.log(props)
  const dispatch = useDispatch();
  const location = useLocation();
  const navigate = useNavigate();

  if (props.authedUser === null) {
    dispatch(setLocation(location.pathname));
    return <Login />;
  }

  const question = props.questions.find((ele) => ele.id === props.id.id);
  function saveAnswer(event) {
    event.preventDefault();
    dispatch(
      setUserAnswer({
        authedUser: props.authedUser.id,
        qid: question.id,
        answer: event.target.value,
      })
    );

    dispatch(
      setPollOption({
        qid: question.id,
        answer: event.target.value,
      })
    );
    dispatch(
      handleQuestionAnswer({
        authedUser: props.authedUser.id,
        qid: question.id,
        answer: event.target.value,
      })
    );

  

    navigate("/home");
  }
  if (Object.keys(props.authedUser.answers).includes(question.id) !== false) {
    const questionVotes = question.optionOne.votes.includes(props.authedUser.id)
        ? question.optionOne.votes.length === 1 ? Lang.personVoted
        : `${question.optionOne.votes.length} ${Lang.peopleVoted}`
        : question.optionTwo.votes.length === 1 ?  Lang.personVoted
        : `${question.optionTwo.votes.length} ${Lang.peopleVoted}`;

    const pieData = [
          {
            name: question.optionOne.text,
            value: question.optionOne.votes.length,
            fill: "#993399",
          },
          {
            name: question.optionTwo.text,
            value: question.optionTwo.votes.length,
            fill: "#4046f5",
          },
      ];
    
    const questionPercentage = question.optionOne.votes.includes(props.authedUser.id)
        ? `${
            (question.optionOne.votes.length / (question.optionOne.votes.length + question.optionTwo.votes.length)) * 100
            }% voted to this option`
        : `${
            (question.optionTwo.votes.length / (question.optionOne.votes.length + question.optionTwo.votes.length)) * 100
          }% voted to this option`;


    const renderCustomizedLabel = ({ x, y, percent }) => {
        return (
          <text x={x} y={y} fill="#4046f5" textAnchor="middle">
            {(percent * 100).toFixed(0)}%
          </text>
        );
      };

    return (
      <div>
        <Header />
        <div className="poll-options-done">
          <h2>
          {question.optionOne.votes.includes(props.authedUser.id)
              ? question.optionOne.text
              : question.optionTwo.text}
          </h2>
          <h2>{questionVotes}</h2>
          <h2>{questionPercentage}</h2>
        </div>
        
          <ResponsiveContainer width="100%" height={300}>
            <PieChart className="poll-piechart" width={100} height={300} >
              <Legend/>
              <Pie
                data={pieData}
                dataKey="value"
                labelLine={false}
                label={renderCustomizedLabel}
                nameKey="name"
                cx="50%"
                cy="50%"
                position="center"
                legendType=""
                innerRadius={20}
              />
            </PieChart>
          </ResponsiveContainer>
      </div>
    );
  }

  return (
    <div>
      <Header />
      <div className="poll-container">
      <div className="poll-header">
          {console.log(props.users)}
          <div>Poll Created by {question.author}</div>
 

      { props.users.map((val,key) =>
        <div key={key}>
            {val.id === question.author ?  <img src={val.avatarURL} className="poll-header-img" alt='user-avatar'/>: ''}
        </div>
      )}
      </div>
        <div className="poll-header">
          <h1>Would you rather</h1>
        </div>
        <div className="poll-options">
              <button
                className="poll-options-item"
                value="optionOne"
                onClick={saveAnswer}>
                {question.optionOne.text}
              </button>
            <button
              className="poll-options-item"
              value="optionTwo"
              onClick={saveAnswer}>
              {question.optionTwo.text}
            </button>
        </div>
      </div>
    </div>
  );
}

function mapStateToProps({ authedUser, questions,users }, props) {
  if (authedUser === null) {
    return {
      authedUser: null,
    };
  }

  const id = props.router.params;
  return {
    id: id,
    users: Object.values(users),
    authedUser,
    questions,
  };

}

export default withRouter(connect(mapStateToProps)(Poll));
