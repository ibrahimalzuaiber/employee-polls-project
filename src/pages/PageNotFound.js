import { connect, useDispatch } from "react-redux";
import { Link, useLocation, useNavigate } from "react-router-dom";
import Header from "../components/header/Header";
import  Lang  from "../constants/Lang";
import { setLocation } from "../redux/actions/index";
import Login from "./Login";


function PageNotFound({authedUser})
{
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const location = useLocation();

    function getBack()
    {
        dispatch(setLocation(location.pathname));
        setTimeout(navigate, 1000, "/home");
    }

    if(authedUser === null)
    {
        dispatch(setLocation(location.pathname));
        return(
            <Login />
        )
    }
    return(
      <>
            <Header/>
          <div className='PageNotFound-container'>
            <div className='PageNotFound-text'>Page Not Found</div>
            <div className='PageNotFound-text'>We couldn't find the page you were looking for.
                    <Link to={"/home"} onClick={getBack} className="PageNotFound-text">{Lang.GETBACK}</Link>
            </div>
          </div>

      </>
  
    )
}


function mapStateToProps({authedUser})
{
    return{
        authedUser
    }
}

export default connect(mapStateToProps)(PageNotFound);