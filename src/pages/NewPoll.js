import Header from "../components/header/Header";
import { useLocation } from "react-router-dom";
import "./style.css";
import { useNavigate } from "react-router-dom";
import { createQuestion } from "../redux/actions/questions";
import { handleCreateQuestion } from "../redux/actions/questions";
import { _saveQuestion } from "../_DATA";
import { setLocation, setUserQuestionArray } from "../redux/actions/index";
import Login from "./Login";
import { connect, useDispatch } from "react-redux";
import  Lang  from "../constants/Lang";

function NewPoll({ authedUser }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const location = useLocation();

  if (authedUser === null) {
    dispatch(setLocation(location.pathname));

    return <Login />;
  }

  function dispatchQuestion(e) {
    e.preventDefault();

    let optionOne = document.getElementById("option-1").value;
    let optionTwo = document.getElementById("option-2").value;

    if (optionOne === "" || optionTwo === "") {
      return alert(Lang.MessageError2);
    }
    _saveQuestion(
      handleCreateQuestion(optionOne, optionTwo, authedUser.id)
    ).then((actualQuestion) => {
      dispatch(
        setUserQuestionArray({
          authedUser: actualQuestion.author,
          qid: actualQuestion.id,
        })
      );
      return dispatch(createQuestion(actualQuestion));
    });
    setTimeout(navigate, 600, "/home");
  }

  return (
    <div>
      <Header />
      <form className="newpoll-container">
        <h1>Would You Rather</h1>
        <h3 className="newpoll-header">Create Your Own Poll</h3>

        <div>
        <input
          type="text"
          placeholder="First Option"
          id="option-1"
          className="newpoll-options"
          data-testid="option-1"
        />
        <input
          type="text"
          placeholder="Second Option"
          id="option-2"
          className="newpoll-options"
          data-testid="option-2"
        />

        </div>
        <button to="/home" className="newpoll-submit" onClick={dispatchQuestion}>
          Submit
        </button>
      </form>
    </div>
  );
}

function mapStateToProps({ authedUser }) {
  if (authedUser === null) {
    return {
      authedUser: null,
    };
  }

  return {
    authedUser,
  };
}

export default connect(mapStateToProps)(NewPoll);
