import React, { useState,useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { logoutUser, setAuthedUser } from "../redux/actions/authUser";
import { connect, useDispatch } from "react-redux";
import { setLocation } from "../redux/actions/index";
import "./style.css";
import PropTypes from "prop-types";
import  Lang  from "../constants/Lang";

function Login({ users, questions, location }) {
  const dispatch = useDispatch();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const [validLocations, setValidLocations] = useState(
    ["/home", "/leaderboard", "/add", "/"].concat(
      questions.map((question) => {
        return `/questions/${question.id}`;
      })
    )
  );
  
  useEffect(() => {
    setValidLocations(["/home","/add", "/leaderboard", "/"].concat(questions.map(question => {return `/question/${question.id}`})))
  }, [questions]) 


  function validUser(e) {
    e.preventDefault();
    const isUser = users.find((obj) => {
      if (obj.id === username && obj.password === password) {
        dispatch(setAuthedUser(obj));
        if(location !== null && validLocations.includes(location))
        {
            navigate(location);
            dispatch(setLocation(null))
            return true;
        }
        if(location !== null)
        {
            dispatch(setLocation(null))
            navigate("*");
            return true
        }
        navigate("/home");
        return true;
      }
      return undefined;
    });

    if (isUser === undefined) {
      dispatch(logoutUser());
      document.getElementById("user").value = "";
      document.getElementById("password").value = "";
      alert(Lang.MessageError1);
    }
  }
  return (
    <form className="loginForm" id="login-form">
      <h1 className="label">Welcome , Login</h1>
      <div className="wrapper">
        <input
          type="text"
          placeholder="Username"
          name="User"
          id="user"
          data-testid="user"
          className="input"
          onChange={(event) => setUsername(event.target.value.trim())}
        />
        <input
          type="password"
          placeholder="Password"
          name="Password"
          id="password"
          data-testid="password"
          className="input"
          onChange={(event) => setPassword(event.target.value.trim())}
        />
      </div>
      <div className="submit">
        <a href="/home" id="home-link" onClick={validUser}>
          Submit
        </a>
      </div>
    </form>
  );
}

Login.propTypes = {
  users: PropTypes.array,
  questions: PropTypes.array,
  location: PropTypes.string,
};

function mapStateToProps({ users, questions, location }) {
  return {
    users: Object.values(users),
    questions,
    location,
  };
}

export default connect(mapStateToProps)(Login);
