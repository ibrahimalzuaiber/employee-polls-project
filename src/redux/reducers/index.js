import { combineReducers } from "redux";
import authedUser from "./authUser";
import userReducer from "./user";
import { questionReducer ,locationReducer} from "./question";

export default combineReducers({
  authedUser,
  users: userReducer,
  questions: questionReducer,
  location: locationReducer,
});
