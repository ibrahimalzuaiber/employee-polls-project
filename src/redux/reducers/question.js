import { SET_POLL_OPTION } from "../actions/authUser";
import { SET_QUESTIONS, CREATE_QUESTION } from "../actions/questions";
import { SET_LOCATION } from "../actions/index";

export function questionReducer(state = [], action)
{
    switch(action.type)
    {
        case SET_QUESTIONS:
            return action.questions;

        case CREATE_QUESTION:
            return [...state, action.payload];

        case SET_POLL_OPTION:
            let {qid, answer, authedUser} = action;
            return state.map(element => element.id === qid ? {
                ...element,
                [answer]: {
                    text: element[answer].text,
                    votes: authedUser !== undefined ? [...element[answer].votes, authedUser] : [...element[answer].votes]
                }
            } : element);

        default:
            return state
    }
}

export function locationReducer(state = null, action)
{
    switch(action.type)
    {
        case SET_LOCATION:
            return action.location;

        default:
            return state;
    }
}
