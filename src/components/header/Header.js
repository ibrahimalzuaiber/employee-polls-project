import { Link , useNavigate} from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { setLocation } from "../../redux/actions/index";
import './style.css';

function Header({authedUser})
{
    const navigate = useNavigate();
    const dispatch = useDispatch();

    function logout()
    {
        dispatch(setLocation(null))
        navigate("/");
    }

    return(
        <div className="nav">
            <div className="nav-left">
               <ul className="nav-items">
                   <Link className="item" to="/home">Home</Link>
                   <Link className="item" to="/leaderboard">Leaderboard</Link>
                </ul>
            </div>
            <div className="nav-right">
               <ul className="nav-items">
                    <div className="nav-items-user">
                        <img src={authedUser.avatar} alt="just an avatar" className="nav-items-user-avatar"/>
                        <h3 className="nav-items-user-name">{authedUser.id}</h3>
                    </div>
                    <Link className="item"  to="/" onClick={logout}>Logout</Link>
                </ul>
                
            </div>
        </div>
    )
}

function mapStateToProps({authedUser})
{
    return{
        authedUser
    }
}

export default connect(mapStateToProps)(Header);