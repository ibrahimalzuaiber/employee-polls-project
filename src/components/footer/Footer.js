import React from "react";
import "./style.css";
import { useNavigate } from "react-router-dom";
import { BsFillPlusCircleFill } from "react-icons/bs";

function Footer() {
  const navigate = useNavigate();

  function addfun(event) {
    navigate(`/add`);
  }
  return (
    <p className="footer-add">
      <h3>
        <BsFillPlusCircleFill size={50} onClick={addfun} color={'rgb(64, 70, 245)'} />
      </h3>
    </p>
  );
}

export default Footer;
