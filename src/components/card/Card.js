import React from "react";
import "./style.css";
import { connect } from "react-redux";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";

function Card({ question }) {
  const navigate = useNavigate();
  const date = new Date(question.timestamp);
  let hours = date.getHours();
  let minutes = date.getMinutes() <= 9 ? `0${date.getMinutes()}` : date.getMinutes();
  let time = `${hours}:${minutes}`;
  let aDate = `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`;
  
  function showfun(event) {
    navigate(`/questions/${event.target.value}`);
  }

  return (
    <div className="card">
      <h2 className="card-title">{question.author}</h2>
      <div className="card-date">
        <p> Time : {time}</p>
        <p> Date : {aDate}</p>
      </div>
      <button className="btn" onClick={showfun} value={question.id}>
        Show
      </button>
    </div>
  );
}

function mapStateToProps({ questions }) {
  return {
    questions,
  };
}

Card.propTypes = {
  question: PropTypes.object,
};

export default connect(mapStateToProps)(Card);
