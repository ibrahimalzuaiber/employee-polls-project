import {setAuthedUser} from "../redux/actions/authUser";
import { MemoryRouter } from "react-router-dom";
import { fireEvent, render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../redux/store";
import { NewPoll } from "../pages";

describe("add", () => {
    it("should render the new poll properly", () => {
        const test = {
            id: "sarahedo",
            avatarURL: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3_juC_cG7iLmwAaK4Mxxy-OtJ52335hZfHhiS5eSHGp9a0C9OY67183F9TV4t01DNILs&usqp=CAU",
            answers: {
                "8xf0y6ziyjabvozdd253nd": 'optionOne',
                "6ni6ok3ym7mf1p33lnez": 'optionOne',
                "am8ehyc8byjqgar0jgpub9": 'optionTwo',
                "loxhs1bqm25b708cmbf3g": 'optionTwo'
              },
            questions: ['8xf0y6ziyjabvozdd253nd', 'am8ehyc8byjqgar0jgpub9']
        }
        store.dispatch(setAuthedUser(test));
        const view = render(
            <MemoryRouter initialEntries={["/add"]}>
                <Provider store={store}>
                    <NewPoll/>
                </Provider>
            </MemoryRouter>
        )

        expect(screen.getByText("Would You Rather")).toBeInTheDocument();
        const inputOne = screen.getByTestId("option-1");
        const inputTwo = screen.getByTestId("option-2");
        fireEvent.change(inputOne, {target: {value: "option one"}});
        fireEvent.change(inputTwo, {target: {value: "option two"}});
        expect(inputOne.value).toEqual("option one");
        expect(inputTwo.value).toEqual("option two");
    })

})