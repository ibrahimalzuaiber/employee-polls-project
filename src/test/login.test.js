
import store from "../redux/store";
import { Login } from "../pages";
import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";

describe("Login", () => {
    it("should render the Login component properly", () => {
        let view = render(
        <Provider store={store}>
            <BrowserRouter>
                <Login />
            </BrowserRouter>
        </Provider>);
        const userName = screen.getByTestId("user");
        const password = screen.getByTestId("password");
        expect(userName).toBeInTheDocument();
        expect(password).toBeInTheDocument();
    })
})