
import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { _getUsers } from "../_DATA";
import { receiveUsers } from "../redux/actions/index";
import { setAuthedUser } from "../redux/actions/authUser";
import { Leaderboard } from "../pages";
import store from "../redux/store";

describe("setAuthedUser", () => {
    it("sshould render the leaderboard properly", async() => {
        const test = {
            id: "sarahedo",
            avatarURL: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3_juC_cG7iLmwAaK4Mxxy-OtJ52335hZfHhiS5eSHGp9a0C9OY67183F9TV4t01DNILs&usqp=CAU",
            answers: {
                "8xf0y6ziyjabvozdd253nd": 'optionOne',
                "6ni6ok3ym7mf1p33lnez": 'optionOne',
                "am8ehyc8byjqgar0jgpub9": 'optionTwo',
                "loxhs1bqm25b708cmbf3g": 'optionTwo'
              },
            questions: ['8xf0y6ziyjabvozdd253nd', 'am8ehyc8byjqgar0jgpub9']
        }
        const testAuth = setAuthedUser(test);
        expect(testAuth).toBeInstanceOf(Object);
        expect(testAuth.id).toBe("sarahedo");
        expect(testAuth.questions.length).toEqual(2);
    })
})

describe("receiveUsers", () => {
    it("should return all of the users", async() => {
        const users = await _getUsers();
        const testUsersArray = ["sarahedo", "tylermcginnis", "mtsamis", "zoshikanlu"];
        expect(users).toBeInstanceOf(Object);
        expect(Object.keys(users)).toEqual(testUsersArray);
    })
})
