import { _saveQuestion, _saveQuestionAnswer, _getUsers, _getQuestions } from "../_DATA";

describe("_saveQuestion", () => {
    it(`"should savee questionpoll properly`, async() => {
            const testQuestion = {
                author: "sarahedo",
                optionOneText: "React js",
                optionTwoText: "React native"
            };

            const actualQuestion = await _saveQuestion(testQuestion);
            expect(actualQuestion.author).toBe("sarahedo");
            expect(actualQuestion.optionOne.text).toBe("React js");
            expect(actualQuestion.optionTwo.text).toBe("React native");
        });

    it("if fail, pass the data as the argument", async() => {
        const testQuestionOne = {};
        const testQuestionTwo = {
            author: "sarahedo",
            optionOne: "React js"
        };

        await expect(_saveQuestion(testQuestionOne)).rejects.toEqual("Please provide optionOneText, optionTwoText, and author");
        await expect(_saveQuestion(testQuestionTwo)).rejects.toEqual("Please provide optionOneText, optionTwoText, and author");
    });
});



describe("_getQuestions", () => {
    it("should redner all questions", async() => {
        const actualQuestions = await _getQuestions();
        const testArray = ["8xf0y6ziyjabvozdd253nd", "6ni6ok3ym7mf1p33lnez", "am8ehyc8byjqgar0jgpub9"];

        expect(actualQuestions).toBeInstanceOf(Object);
        expect(Object.keys(actualQuestions)).toEqual(expect.arrayContaining(testArray));
        expect(actualQuestions[testArray[0]].author).toBe("sarahedo");
    });
});

describe("_getUsers", () => {
    it("should render all Users", async() => {
        const objects = ["sarahedo", "tylermcginnis", "mtsamis", "zoshikanlu"];
        const users = await _getUsers();
        const keys = Object.keys(users);
        await expect(_getUsers).toBeInstanceOf(Object);
        expect(keys.sort()).toEqual(objects.sort());
    });
});

describe("_saveQuestionAnswer", () => {
    it(`should update the user with user answer`, async() => {
            const testUser = {
                authedUser: "sarahedo",
                qid: "8xf0y6ziyjabvozdd253nd",
                answer: "optionOne"
            };

            const actualAnswer = await _saveQuestionAnswer(testUser);
            expect(actualAnswer).toBe(true);
    });

    it("if fail, pass the data as the argument", async() => {
        const testUserOne = {
            authedUser: "sarahedo",
            qid: "8xf0y6ziyjabvozdd253nd"
        };

        const testUserTwo = [];

        await expect(_saveQuestionAnswer(testUserOne)).rejects.toEqual("Please provide authedUser, qid, and answer");
        await expect(_saveQuestionAnswer(testUserTwo)).rejects.toEqual("Please provide authedUser, qid, and answer");
    });
});
