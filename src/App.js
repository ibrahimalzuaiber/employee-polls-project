import "./App.css";
import { Route, Routes } from "react-router-dom";
import React, { Fragment, useEffect } from "react";
import { NewPoll, PageNotFound, Home, Login, Leaderboard, Poll } from "./pages";
import { useDispatch } from "react-redux";
import { receiveUsers } from "./redux/actions/index";
import { setQuestions } from "./redux/actions/questions";
import { _getUsers, _getQuestions } from "./_DATA";

function App() {
  const dispatch = useDispatch();
  function handleInitialData() {
    _getQuestions().then((data) => dispatch(setQuestions(data)));
    _getUsers().then((data) => dispatch(receiveUsers(data)));
  }
  useEffect(() => {
    handleInitialData();
  });

  return (
    <Fragment>
      <Routes>
        <Route path='/' element={<Login />} />
        <Route path='/home' element={<Home />} />
        <Route path='/add' element={<NewPoll />} />
        <Route path='/questions/:id' element={<Poll />} />
        <Route path='/leaderboard' element={<Leaderboard />} />
        <Route path='*' element={<PageNotFound />} />
      </Routes>
    </Fragment>
  );
}

export default App;
