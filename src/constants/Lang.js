const Lang =  {
    done: 'Answered',
    NewQuestion: 'Unanswered',
    show: 'Show',
    LEADERBOARD:'LEADERBOARD',
    HOME:'HOME',
    LOGOUT:'LOGOUT',
    UserName:'User Name',
    avatar:'Avatar',
    Answered:'Answered',
    Created:'Created',
    WouldYouRather:'Would You Rather',
    CreateYourPoll:'Create Your Own Poll',
    FOption:'First Option',
    SOption:'Second Option',
    MessageError1:'The username or password is not correct. Please try again',
    MessageError2:'Please fill out this options',
    submit:'Submit',
    GETBACK:'Back to Home Page',
    personVoted:'Only one person voted to this option',
    peopleVoted:'People voted to this option',

  };

  export default Lang